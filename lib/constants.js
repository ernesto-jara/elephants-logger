'use strict'

var today = new Date().toISOString().split('T')

module.exports = {
  ERROR: 0,
  WARN: 1,
  INFO: 2,
  VERBOSE: 3,
  DEBUG: 4,
  HTTP: 5,
  COLOR_ERROR: 'red',
  COLOR_WARN: 'orange',
  COLOR_INFO: 'white bold yellow',
  COLOR_VERBOSE: 'blue',
  COLOR_DEBUG: 'green',
  COLOR_HTTP: 'pink',
  LEVEL: process.env.NODE_ENV === 'production' ? 'error' : 'debug',
  DEBUG_FILE: process.cwd() + '/logs/debug_' + today[0] + '.log',
  ERROR_FILE: process.cwd() + '/logs/error_' + today[0] + '.log',
}
