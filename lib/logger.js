'use strict'

var { logger } = require('./winston')

function Logger() {
  this.logger = logger
}

Logger.prototype.singleLine = function(message) {
  this.logger.debug(message)
}

Logger.prototype.entryMethod = function(verb, method, path) {
  this.singleLine('[38;5;214m[1m[ [22m[39m' + verb + '[38;5;214m[1m ] [22m[39m' + path)
  this.singleLine('METHOD: [38;5;44m[1m' + method + '[22m[39m')
}

Logger.prototype.logInfoObject = function(obj) {
  if (Object.keys(obj).length > 0) {
    Object.keys(obj).forEach(key => {
      this.logger.info('PROPERTY: [38;5;135m[1m' + obj[key] + '[22m[39m')
    })
  }
}

Logger.prototype.logInfoString = function(message) {
  this.logger.info(message)
}

Logger.prototype.logDebugObject = function(obj) {
  if (Object.keys(obj).length > 0) {
    Object.keys(obj).forEach(key => {
      this.logger.debug('PROPERTY: [38;5;135m[1m' + obj[key] + '[22m[39m')
    })
  }
}

Logger.prototype.logDebugString = function(message) {
  this.logger.debug(message)
}

Logger.prototype.logErrorObject = function(obj) {
  if (Object.keys(obj).length > 0) {
    Object.keys(obj).forEach(key => {
      this.logger.error('PROPERTY: [38;5;135m[1m' + obj[key] + '[22m[39m')
    })
  }
}

Logger.prototype.logErrorString = function(message) {
  this.logger.error(message)
}

Logger.prototype.logWarnObject = function(obj) {
  if (Object.keys(obj).length > 0) {
    Object.keys(obj).forEach(key => {
      this.logger.warn('PROPERTY: [38;5;135m[1m' + obj[key] + '[22m[39m')
    })
  }
}

Logger.prototype.logWarnString = function(message) {
  this.logger.error(message)
}

Logger.prototype.timeSpendToResolve = function() {
  var start = Date.now()
  logger.debug(new Date())
  return () => {
    var endDate = Date.now()
    this.logger.debug('time to complete request: [38;5;214m[1m' + ((endDate - start) / 1000 + '[22m[39m seconds'))
  }
}

exports.Logger = Logger
