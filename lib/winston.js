'use strict'

var winston = require('winston')
var constantans = require('./constants')

var { colorize, combine, timestamp, printf, json } = winston.format

var custom = {
  levels: {
    error: constantans.ERROR,
    warn: constantans.WARN,
    info: constantans.INFO,
    verbose: constantans.VERBOSE,
    debug: constantans.DEBUG,
    http: constantans.HTTP,
  },
  colors: {
    error: constantans.COLOR_ERROR,
    warn: constantans.COLOR_WARN,
    info: constantans.COLOR_INFO,
    verbose: constantans.COLOR_VERBOSE,
    debug: constantans.COLOR_DEBUG,
    http: constantans.COLOR_HTTP,
  },
}

winston.addColors(custom.colors)
var logFormat = printf(info => {
  return `[${info.timestamp}] [${info.level}] [38;5;13m[1m=>[22m[39m ${info.message}`
})

var logger = winston.createLogger({
  levels: custom.levels,
  level: constantans.LEVEL,
  format: combine(timestamp(), colorize({ colors: custom.colors }), json(), logFormat),

  transports: [
    new winston.transports.File({ filename: constantans.DEBUG_FILE, level: 'debug' }),
    new winston.transports.File({ filename: constantans.ERROR_FILE, level: 'error' }),
    new winston.transports.Console(),
  ],
})

exports.logger = logger
