declare module '@elephant/logger' {
    class Logger {
        public constructor ()

        public entryMethod(verb: string, method: string, path: string): void
        public logInfoObject<Obj = never>(obj: Obj): void
        public logInfoString(message: string): void
        public logDebugObject<Obj = never>(obj: Obj): void
        public logDebugString(message: string): void
        public logErrorObject<Obj extends Error>(obj: Obj): void
        public logErrorString(message: string): void
        public logWarnObject<Obj = never>(obj: Obj): void
        public logWarnString(message: string): void
        public timeSpendToResolve(): () => void
    }
}
